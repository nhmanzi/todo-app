import React from "react";
import { connect } from "react-redux";
import { addTodo } from "../actions";

const AddTodo = ({ dispatch }) => {
  let input;

  return (
    <div>
      <form
        className="to-do-form"
        onSubmit={(e) => {
          e.preventDefault();
          if (!input.value.trim()) {
            return;
          }
          dispatch(addTodo(input.value));
          input.value = "";
        }}
      >
        <input
          placeholder="what are you planning...."
          ref={(node) => (input = node)}
        />
        <button type="submit">ADD</button>
      </form>
    </div>
  );
};

export default connect()(AddTodo);
