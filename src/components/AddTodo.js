import React, { Component } from "react";
import { connect } from "react-redux";

class AddTodo extends Component {
  render() {
    return (
      <div>
        <div id="to-do-form">
          <input
            type="text"
            placeholder="Enter Something...."
            onChange={(e) => this.props.handleChange(e.target)}
          ></input>
          <button
            onClick={() => {
              this.props.addTodo(this.props.input);
            }}
          >
            add
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleChange: (target) => {
      dispatch({ type: "SET_INPUT", value: target.value });
    },
    addTodo: (input) => {
      let obj = {};
      let inputvalue = input;
      let id = Math.floor(Math.random() * 1000);
      obj = { inputvalue, id };

      dispatch({ type: "ADD_TODO", value: obj });
    },
  };
};

const mapStateToProps = (state) => {
  return {
    input: state.input,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);
