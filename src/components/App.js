import React from "react";
import Footer from "./Footer";
import Header from "./header";
import "../App.css";
import AddTodo from "../containers/AddTodo";
import VisibleTodoList from "../containers/VisibleTodoList";

const App = () => (
  <div className="container">
    <Header />
    <AddTodo />
    <VisibleTodoList />
    <Footer />
  </div>
);

export default App;
