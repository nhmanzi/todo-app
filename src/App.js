import React from "react";
<<<<<<< HEAD
import Footer from "./Footer";

import AddTodo from "../containers/AddTodo";
import "./App.css";
import VisibleTodoList from "../containers/VisibleTodoList";

const App = () => (
  <div>
    <title />
    <AddTodo />
    <VisibleTodoList />
    <Footer />
  </div>
);

export default App;
=======
import "./App.css";
// import Todos from "./components/todos";
import AddTodo from "./components/AddTodo";
import { connect } from "react-redux";
import { addTodo, toggleTodo, delTodo } from "./actions";

function App() {
  return (
    // moved provider to root component(index.js)
    // this is because when a component is wrapped in connect functions
    // it  expects the parent component to have a Provider
    // so the mistake was that both were in the same file.
    <div className="wrapper">
      <h1 style={{ fontSize: "14", textAlign: "center" }}>TODO LIST</h1>
      <AddTodo AddTodo={addTodo} />
      {/* here I commented out the todos component because it references state and there's no state here. inspect and find the appropriate fix */}
      {/* <Todos
          todos={this.state.todos}
          markComplete={this.markComplete}
          delTodo={this.delTodo}
        /> */}
    </div>
  );
}

const mapStateToProps = (state = []) => {
  return {
    title: state.title,
    id: state.id,
  };
};

const mapDispatchToProps = () => {
  return {
    AddTodo: addTodo,
    delTodo,
    markComplete: toggleTodo,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(App);
>>>>>>> 456f1c15783ce59a66e5d3e21c6fe0116a62192a
